{{- $repositoryUrl := printf "$(params.%s)" .Values.parameters.repositoryUrl.name }}
{{- $repositoryRef := printf "$(params.%s)" .Values.parameters.repositoryRef.name }}
{{- $projectName := printf "$(params.%s)" .Values.parameters.projectName.name }}

{{- $taskConfigReader := "read-build-configuration" }}
{{- $taskBuildImage := "build-image" }}

{{- $contextDir := printf "$(tasks.%s.results.%s)" $taskConfigReader .Values.configurationReader.build.source.contextDir.resultName }}
{{- $imageRegistry := printf "$(tasks.%s.results.%s)" $taskConfigReader .Values.configurationReader.build.source.imageRegistry.resultName }}
{{- $imageRepository := printf "$(tasks.%s.results.%s)" $taskConfigReader .Values.configurationReader.build.source.imageRepository.resultName }}
{{- $dockerfile := printf "$(tasks.%s.results.%s)" $taskConfigReader .Values.configurationReader.build.source.dockerfile.resultName }}

{{- $imageTagInitial := "dev" }}

{{- $subDirectory := printf "%s-%s" $projectName $repositoryRef }}
{{- $sourceCodeDir := printf "%s/%s" $subDirectory $contextDir }}
{{- $imageRegistryPlusRepository := printf "%s/%s" $imageRegistry $imageRepository}}
{{- $initialFullImageName := printf "%s:%s" $imageRegistryPlusRepository $imageTagInitial }}


apiVersion: tekton.dev/v1beta1
kind: Pipeline
metadata:
  name: {{ .Values.pipelines.semanticRelease.docker.name }}
spec:
  description: |
    Pipeline to build a Docker Image
  workspaces:
  {{- range $sub, $workspace := .Values.pipelines.semanticRelease.docker.workspaces }}
  - name: {{ $workspace.name }}
  {{- end }}

  params:
  {{- range $sub, $parameter := .Values.pipelines.semanticRelease.docker.parameters }}
  - name: {{ $parameter.name }}
    description: {{ $parameter.description }}
  {{- if $parameter.default }}
    default: {{ $parameter.default }}
  {{- end }}
  {{- end }}
  tasks:

  # 1: Clone Source Repository
  - name: clone-source-repository
    taskRef:
      name: {{ .Values.tasks.names.git.clone }}
    params:
    - name: url
      value: {{ $repositoryUrl }}
    - name: revision
      value: {{ $repositoryRef }}
    - name: deleteExisting
      value: 'true'
    - name: subdirectory
      value: {{ $subDirectory }}
    workspaces:
    - name: output
      workspace: {{ .Values.workspaces.sourcePVC.name }}

  # Step 2: Read the contents from the Tektonfile
  - name: {{ $taskConfigReader }}
    taskRef:
      name: {{ .Values.tasks.names.tekton.readBuildTektonfile }}
    params:
    - name: context-dir
      value: {{ $subDirectory }}
    runAfter:
    - clone-source-repository
    workspaces:
    - name: source
      workspace: {{ .Values.workspaces.sourcePVC.name }}

  # Step 3: Build the Image with the artifact
  - name: build-image
    taskRef:
      name: {{ .Values.tasks.names.docker.buildah }}
    runAfter:
    - {{ $taskConfigReader }}
    params:
    - name: IMAGE
      value: {{ $initialFullImageName }}
    - name: DOCKERFILE
      value: {{ $dockerfile }}
    - name: CONTEXT
      value: {{ $sourceCodeDir }}
    - name: TLSVERIFY
      value: "false"
    workspaces:
    - name: source
      workspace: {{ .Values.workspaces.sourcePVC.name }}

# Step 4: Run semantic release?
  - name: semantic-release
    taskRef:
      name: {{ .Values.tasks.names.semanticRelease.semanticRelease }}
    runAfter:
      - build-image
    params:
    - name: git-branch
      value: {{ $repositoryRef }}
    - name: context-dir
      value: {{ $subDirectory }}
    - name: initial-registry
      value: {{ $imageRegistry }}
    - name: initial-repository
      value: {{ $imageRepository }}
    - name: initial-tag
      value: {{ $imageTagInitial }}
    - name: final-registry
      value: {{ $imageRegistry }}
    - name: final-repository
      value: {{ $imageRepository }}
    workspaces:
    - name: source
      workspace: {{ .Values.workspaces.sourcePVC.name }}
{{/*
Expand the name of the chart.
*/}}
{{- define "tekton-cicd-v2.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "tekton-cicd-v2.fullname" -}}
{{- if .Values.fullnameOverride }}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- $name := default .Chart.Name .Values.nameOverride }}
{{- if contains $name .Release.Name }}
{{- .Release.Name | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" }}
{{- end }}
{{- end }}
{{- end }}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "tekton-cicd-v2.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Common labels
*/}}
{{- define "tekton-cicd-v2.labels" -}}
helm.sh/chart: {{ include "tekton-cicd-v2.chart" . }}
{{ include "tekton-cicd-v2.selectorLabels" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end }}

{{/*
Selector labels
*/}}
{{- define "tekton-cicd-v2.selectorLabels" -}}
app.kubernetes.io/name: {{ include "tekton-cicd-v2.name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}

{{/*
Create the name of the service account to use
*/}}
{{- define "tekton-cicd-v2.serviceAccountName" -}}
{{- if .Values.serviceAccount.create }}
{{- default (include "tekton-cicd-v2.fullname" .) .Values.serviceAccount.name }}
{{- else }}
{{- default "default" .Values.serviceAccount.name }}
{{- end }}
{{- end }}


{{- define "workspaces.list" }}
{{- range $sub, $workspace := .workspaces }}
- name: {{ $workspace.name }}
{{- if $workspace.configMap }}
  configMap:
    name: {{ $workspace.configMap.name }}
{{- else if $workspace.pvc }}
  volumeClaimTemplate:
    spec:
    {{- with $workspace.pvc.accessModes }}
      accessModes:
      {{- toYaml . | nindent 6 }}
    {{- end }}
      resources:
        requests:
          storage: {{ $workspace.pvc.capacity }}
{{- else if $workspace.pvcFixed }}
  persistentVolumeClaim:
    claimName: {{ $workspace.pvcFixed.claimName }}
{{- else if $workspace.secret }}
  secret:
    secretName: {{ $workspace.secret.name }}
{{- end }}
{{- end }}
{{- end }}
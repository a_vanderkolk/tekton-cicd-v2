apiVersion: tekton.dev/v1beta1
kind: Pipeline
metadata:
  name: {{ .Values.pipelines.build.java.springBoot.name }}
spec:
  description: |
    Pipeline to build a Spring Boot application image
  workspaces:
  {{- range $sub, $workspace := .Values.pipelines.build.java.springBoot.workspaces }}
  - name: {{ $workspace.name }}
  {{- end }}

  params:
  {{- range $sub, $parameter := .Values.pipelines.build.java.springBoot.parameters }}
  - name: {{ $parameter.name }}
    description: {{ $parameter.description }}
  {{- if $parameter.default }}
    default: {{ $parameter.default }}
  {{- end }}
  {{- end }}
  tasks:
  {{- $parametersSource := .Values.parameters }}

  # 1: Clone Source Repository
  - name: clone-source-repository
    taskRef:
      name: {{ .Values.tasks.names.git.clone }}
    params:
    - name: url
      value: {{ printf "$(params.%s)" $parametersSource.repositoryUrl.name }}
    - name: revision
      value: {{ printf "$(params.%s)" $parametersSource.repositoryRef.name }}
    - name: deleteExisting
      value: 'true'
    workspaces:
    # TODO: dynamisch maken
    - name: output
      workspace: {{ .Values.workspaces.sourcePVC.name }}

  # Step 2A: Read the contents from the Tektonfile
  {{- $tektonfileReaderTaskName := "read-build-configuration" }}
  {{- $tektonfileReaderParametersSource := .Values.configurationReader.build.source }}
  - name: {{ $tektonfileReaderTaskName }}
    taskRef:
      name: {{ .Values.tasks.names.tekton.readBuildTektonfile }}
    runAfter:
    - clone-source-repository
    workspaces:
    - name: source
      workspace: {{ .Values.workspaces.sourcePVC.name }}

  # Step 2B: Run semantic release?
  - name: semantic-release
    taskRef:
      name: {{ .Values.tasks.names.semanticRelease.semanticRelease }}
    runAfter:
      - clone-source-repository
    params:
    - name: gitea-token
      value: lalalalalala
    - name: git-branch
      value: {{ printf "$(params.%s)" $parametersSource.repositoryRef.name }}
    workspaces:
    # TODO: dynamisch maken
    - name: source
      workspace: {{ .Values.workspaces.sourcePVC.name }}

  # Step 3A: Get the application version from the pom file
  {{- $pomReaderTaskName := "read-project-details" }}
  - name: {{ $pomReaderTaskName }}
    taskRef:
      name: {{ .Values.tasks.names.maven.pomReader }}
    runAfter:
    - semantic-release
    params:
    - name: context-dir
      value: {{ printf "$(tasks.%s.results.%s)" $tektonfileReaderTaskName $tektonfileReaderParametersSource.contextDir.resultName }}
    workspaces:
    - name: source
      workspace: {{ .Values.workspaces.sourcePVC.name }}

  # Step 3B: Run Unit Tests
  - name: maven-test
    taskRef:
      name: {{ .Values.tasks.names.maven.maven }}
    runAfter:
    - semantic-release
    - {{ $tektonfileReaderTaskName }}
    params:
    - name: CONTEXT_DIR
      value: {{ printf "$(tasks.%s.results.%s)" $tektonfileReaderTaskName $tektonfileReaderParametersSource.contextDir.resultName }}
    - name: GOALS
      value:
      - '-B'
      - 'clean'
      - 'test'
      - '-Dmaven.repo.local={{ printf "$(workspaces.%s.path)/.m2" .Values.workspaces.sourcePVC.name }}'
    workspaces:
    - name: source
      workspace: {{ .Values.workspaces.sourcePVC.name }}
    - name: maven-settings
      workspace: {{ .Values.workspaces.mavenSettingsXML.name }}

  # Step 4: Build the artifact
  - name: maven-package
    taskRef:
      name: {{ .Values.tasks.names.maven.maven }}
    runAfter:
    - maven-test
    params:
    - name: CONTEXT_DIR
      value: {{ printf "$(tasks.%s.results.%s)" $tektonfileReaderTaskName $tektonfileReaderParametersSource.contextDir.resultName }}    
    - name: GOALS
      value:
      - '-B'
      - '-DskipTests'
      - 'clean'
      - 'package'
      - '-Dmaven.repo.local={{ printf "$(workspaces.%s.path)/.m2" .Values.workspaces.sourcePVC.name }}'
    workspaces:
    - name: source
      workspace: {{ .Values.workspaces.sourcePVC.name }}
    - name: maven-settings
      workspace: {{ .Values.workspaces.mavenSettingsXML.name }}

  # Step 8: Build the Image with the artifact
  {{- $imageRegistryResult := printf "$(tasks.%s.results.%s)" $tektonfileReaderTaskName $tektonfileReaderParametersSource.imageRegistry.resultName }}
  {{- $imageRepositoryResult := printf "$(tasks.%s.results.%s)" $tektonfileReaderTaskName $tektonfileReaderParametersSource.imageRepository.resultName }}
  {{- $appVersionResult := printf "$(tasks.%s.results.%s)" $pomReaderTaskName .Values.pomReader.version.resultName }}
  {{- $fullImageName := printf "%s/%s:%s" $imageRegistryResult $imageRepositoryResult $appVersionResult }}
  - name: build-image
    taskRef:
      name: {{ .Values.tasks.names.docker.buildah }}
    runAfter:
    - maven-package
    - {{ $pomReaderTaskName }}
    params:
    - name: IMAGE
      value: {{ $fullImageName }}
    - name: DOCKERFILE
      value: {{ printf "$(tasks.%s.results.%s)" $tektonfileReaderTaskName $tektonfileReaderParametersSource.dockerfile.resultName }}
    - name: CONTEXT
      value: {{ printf "$(tasks.%s.results.%s)" $tektonfileReaderTaskName $tektonfileReaderParametersSource.contextDir.resultName | quote }}
    - name: TLSVERIFY
      value: "false"
    - name: BUILD_EXTRA_ARGS
      value: --label quay.expires-after=90d
    workspaces:
    - name: source
      workspace: {{ .Values.workspaces.sourcePVC.name }}
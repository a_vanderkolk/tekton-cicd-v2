#!/bin/bash

mkdir -p ./templates/configuration
mkdir -p ./templates/tasks/docker
mkdir -p ./templates/tasks/maven
mkdir -p ./templates/tasks/cli
mkdir -p ./templates/tasks/helm
mkdir -p ./templates/tasks/git/gitea
mkdir -p ./templates/tasks/tekton
mkdir -p ./templates/tasks/k8s
mkdir -p ./templates/tasks/argocd
mkdir -p ./templates/tasks/semanticversioning


mkdir -p ./templates/pipelines/build/spring
mkdir -p ./templates/pipelines/build/quarkus
mkdir -p ./templates/pipelines/build/java
mkdir -p ./templates/pipelines/build/angular
mkdir -p ./templates/pipelines/deploy/helm


mkdir -p ./templates/triggers/build/spring
mkdir -p ./templates/triggers/build/quarkus
mkdir -p ./templates/triggers/build/java
mkdir -p ./templates/triggers/build/angular
mkdir -p ./templates/triggers/deploy/helm


mkdir -p ./templates/workspaces
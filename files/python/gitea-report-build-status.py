#!/usr/bin/env python3

"""
Comments the build status to the PR
"""

import requests
import os
import json
import logging
from datetime import datetime


def get_organization(repository_url):
    """Retrioeve the organization from the Repository Url

    Args:
        repository_url (string): The url to clone the reposiotry with

    Returns:
        string: The 'organization' from Gitea Clone URL
    """

    return repository_url.split('/')[3]


def get_repository(repository_url):
    """Retrieve the Repository Name from the Repository URL

    Args:
        repository_url (string): The url to clone the reposiotry with

    Returns:
        [type]: The 'repository' from Gitea Clone URL
    """
    return repository_url.split('/')[4].replace(".git", "")


def create_url(gitea_url, organization, repository, pull_request_id):
    """[summary]

    Args:
        gitea_url (string): The Base URL from the Gitea instance
        organization (string): The name of the organization in Gitea
        repository (string): The repository name
        pull_request_id (int): The ID of the pull request

    Returns:
        string: The URL to the endpoint to push comments to a Pull Request
    """
    return "{}/api/v1/repos/{}/{}/pulls/{}/reviews".format(gitea_url, organization, repository, pull_request_id)


def get_headers(token):
    """Builds the headers object to be able to call the endpoint

    Args:
        token (string): The personal access token of the user to call the endpoint with

    Returns:
        dict: The Header for sending a Comment to a PR
    """
    return {
        'Authorization': "token {}".format(token),
        'accept': 'application/json',
        'Content-Type': 'application/json'}


def create_payload(body):
    """Creates the payload for sending to the endpoint

    Args:
        body (string): The formatted message to send to publish on the Pull Request

    Returns:
        dict: The body for the request
    """
    logging.info("Creating message: %s", body)
    return {
        'body': body
    }


def create_message(application_name,
                   application_version,
                   build_status,
                   sonar_status,
                   sonar_url,
                   image_url,
                   image_sha,
                   iac_repository,
                   iac_branch):
    pattern = "| {} | {} |"

    table_header = pattern.format("", "Value")
    seperator = pattern.format(":---", ":---")

    time_line = pattern.format("Timestamp build", datetime.now())
    application_name_line = pattern.format("Application", application_name)
    application_version_line = pattern.format("Version", application_version)
    build_status_line = pattern.format("Build Status", build_status)
    sonar_status_line = pattern.format("Sonar Status", sonar_status)
    sonar_dashboard_line = pattern.format("Sonar Dashboard", sonar_url)
    image_url_line = pattern.format("Image", image_url)
    image_sha_line = pattern.format("Image SHA", image_sha)
    iac_repository_line = pattern.format("IaC Repository", iac_repository)
    iac_branch_line = pattern.format("IaC branch", iac_branch)

    return "\n".join([
        table_header,
        seperator,
        time_line,
        application_name_line,
        application_version_line,
        build_status_line,
        sonar_status_line,
        sonar_dashboard_line,
        image_url_line,
        image_sha_line,
        iac_repository_line,
        iac_branch_line])


def call_endpoint(url, headers, payload):
    logging.info("Calling url: %s", url)

    resp = requests.post(url, json=payload, headers=headers)

    if resp.status_code != 200 and resp.status_code != 201:
        logging.error("Error calling endpoint (status code: {})".format(
            resp.status_code))
        pretty_print = json.dumps(resp.json(), indent=4)
        logging.error(pretty_print)
        raise Exception("Unable to post build status")
    else:
        return resp


def report_build_status():
    logging.basicConfig(level=logging.INFO)
    token = os.environ['AUTH_TOKEN']
    repository_url = "$(params.repository-url)"
    gitea_url = "$(params.gitea-url)"
    pull_request_id = "$(params.pull-request-id)"

    application_name = "$(params.application-name)"
    application_version = "$(params.application-version)"
    build_status = "$(params.build-status)"
    sonar_status = "$(params.sonar-status)"
    sonar_dashboard = "$(params.sonar-dashboard)"
    image_url = "$(params.image-name)"
    image_sha = "$(params.image-sha)"
    iac_repository = "$(params.iac-repository)"
    iac_branch = "$(params.iac-branch)"

    # TODO: Delete me
    # token = "aaaa"
    # repository_url = "ssh://git@192.168.178.21:12022/gitopsdemo/spring-demo-app.git"
    # gitea_url = "http://192.168.178.21:12080"
    # application_name = "My demo app"
    # application_version = "1.0.0-SNAPSHOT"
    # build_status = "OK"
    # sonar_status = "OK"
    # sonar_dashboard = "http://sonarqube.labs.kolkos.nl/dashboard?id=my-demo-app"
    # iac_repository = "ssh://git@192.168.178.21:12022/gitopsdemo/spring-demo-app-infra.git"
    # iac_branch = "feature/implementh2"
    # pull_request_id = "3"

    organization = get_organization(repository_url)
    repository = get_repository(repository_url)
    url = create_url(gitea_url, organization, repository, pull_request_id)
    headers = get_headers(token)
    message = create_message(application_name,
                             application_version,
                             build_status,
                             sonar_status,
                             sonar_dashboard,
                             image_url,
                             image_sha,
                             iac_repository,
                             iac_branch)
    payload = create_payload(message)

    call_endpoint(url, headers, payload)


report_build_status()

#!/usr/bin/env python3

"""
Script to create a PR for Source Repositories
"""

import requests
import os
import json


def get_organization(repository_url):
    return repository_url.split('/')[3]


def get_repository(repository_url):
    return repository_url.split('/')[4].replace(".git","")


def create_pull_request_url(gitea_url, organization, repository):
    return "{}/api/v1/repos/{}/{}/pulls".format(gitea_url, organization, repository)


def get_body(title, body, source, target):
    return {
        'base': target,
        'head': source,
        'body': body,
        'title': title
    }


def get_headers(token):
    return {
        'Authorization': "token {}".format(token),
        'accept': 'application/json',
        'Content-Type': 'application/json'}


def main():
    # Change me
    repository_url = "$(params.repository-url)"
    gitea_url = "$(params.gitea-url)"
    title = "$(params.title)"
    body = "$(params.body)"
    source = "$(params.source-branch)"
    target = "$(params.target-branch)"
    token = os.environ['AUTH_TOKEN']

    # TODO: Delete me
    # repository_url = "ssh://git@192.168.178.21:12022/gitopsdemo/demo-web-app-infra.git"
    # gitea_url = r"http://192.168.178.21:12080"
    # title = "My test title"
    # body = "Created using test setup"
    # source = "feature/prtest"
    # target = "development"
    # token = os.environ['AUTH_TOKEN']

    organization = get_organization(repository_url)
    repository = get_repository(repository_url)

    pull_request_url = create_pull_request_url(
        gitea_url, organization, repository)
    request_body = get_body(title, body, source, target)
    headers = get_headers(token)

    response = requests.post(
        pull_request_url, headers=headers, json=request_body)

    if response.status_code != 200 and response.status_code != 201:
        error_message = "Error configuring the webhook (status code: {})".format(
            response.status_code)

        data = response.json()
        pretty_print = json.dumps(data, indent=4)
        print(error_message)
        print(pretty_print)
        raise Exception(str(error_message))
    else:
        print("Successfully Created Pull Request")


main()
